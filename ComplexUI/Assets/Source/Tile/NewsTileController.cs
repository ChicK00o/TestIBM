﻿
using System;
using Prime31.ZestKit;
using UnityEngine;
using UnitySuiceCommons.EventDispatcher;

namespace ComplexUI
{
    public class NewsTileController : INewsTileController
    {
        private INewsTileFactory _factory;
        private INewsTile _tile;
        private ICustomTouchHandler _touchHandler;
        private IGlobalEventDispatcher _eventDispatcher;

        public NewsTileController(INewsTileFactory factory, INewsTile tile, ICustomTouchHandler touchHandler,
            IGlobalEventDispatcher eventDispatcher)
        {
            _factory = factory;
            _tile = tile;
            _touchHandler = touchHandler;
            _eventDispatcher = eventDispatcher;
        }

        public Transform transform { get { return _tile.gameObject.transform; } }

        public void SetPosition(Vector3 position)
        {
            _tile.gameObject.transform.localPosition = position;
        }

        public void SetActive(bool active)
        {
            _tile.gameObject.SetActive(active);
        }

        public void SetParentTransform(Transform parentTransform, bool worldStay)
        {
            _tile.gameObject.transform.SetParent(parentTransform, worldStay);
        }

        public void SetValues(float renderZ, float alpha)
        {
            SetPosition(new Vector3(0, 0, renderZ));
            _tile.GetGanvasHandler.alpha = alpha;
        }

        public Vector2 GetSize()
        {
            return (transform as RectTransform).sizeDelta;
        }

        public void SetTileData(Sprite sprite, Color color, string headlineText, string sysnopsisText)
        {
            SetPressedHandler();
            _tile.SetTileData(sprite, color, headlineText, sysnopsisText);
        }

        private void SetPressedHandler()
        {
            _tile.GetPressGesture.Pressed += OnPressed;
            _tile.GetReleaseGesture.Released += OnReleased;
            _tile.GetTapGesture.Tapped += OnTapped;
        }

        public void TweenTo(Vector3 position, EaseType type, LoopType loop = LoopType.None, float duration = 1.0f)
        {
            transform.ZKlocalPositionTo(position, duration).setEaseType(type).setLoops(loop).start();
        }

        public void ScaleTweenTo(float scale)
        {
            transform.ZKlocalScaleTo(new Vector3(scale,scale,scale), 1f).start();
        }

        public void RotationTweenTo(Vector3 rotation)
        {
            transform.ZKlocalRotationTo(Quaternion.Euler(rotation), 1f).start();
        }

        public void SetSelectionMode(bool active)
        {
            _tile.SetSelectionMode(active);
        }

        public void SetupForReaderMode(bool inToReaderMode)
        {
            _tile.GetGanvasHandler.interactable = inToReaderMode;
        }

        private void OnTapped(object sender, EventArgs e)
        {
            _eventDispatcher.BroadcastEvent(new DoubleTapReadEvent(this));
        }

        private void OnReleased(object sender, EventArgs e)
        {
            _touchHandler.OnReleased();
        }

        private void OnPressed(object sender, EventArgs e)
        {
            _touchHandler.OnPressed();
        }

        public void Destroy()
        {
            RemovePressedHandler();
            _factory.ReturnToPool(this);
        }

        private void RemovePressedHandler()
        {
            _tile.GetPressGesture.Pressed -= OnPressed;
            _tile.GetReleaseGesture.Released -= OnReleased;
            _tile.GetTapGesture.Tapped -= OnTapped;
        }
    }
}

