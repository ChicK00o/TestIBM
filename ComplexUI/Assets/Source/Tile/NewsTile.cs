﻿using System;
using Prime31.ZestKit;
using TouchScript.Gestures;
using UnityEngine;
using UnityEngine.UI;

namespace ComplexUI
{
    public class NewsTile : MonoBehaviour, INewsTile
    {
        public Image HighlightImage;
        public Image OverlayImage;
        public Image HeadDividerLine;
        public Text HeadLineText;

        [SerializeField]
        private NewsTileHighlightBehaviour _synopsisHandler;

        [SerializeField]
        private CanvasGroup _canvasHandler;

        [SerializeField]
        private PressGesture _pressGesture;

        [SerializeField]
        private ReleaseGesture _releaseGesture;

        [SerializeField]
        private TapGesture _tapGesture;

        public CanvasGroup GetGanvasHandler { get { return _canvasHandler; } }
        public PressGesture GetPressGesture { get { return _pressGesture; } }
        public ReleaseGesture GetReleaseGesture { get { return _releaseGesture; } }
        public TapGesture GetTapGesture { get { return _tapGesture; } }

        public void SetTileData(Sprite sprite, Color color, string headlineText, string sysnopsisText)
        {
            HighlightImage.sprite = sprite;
            float alpha = OverlayImage.color.a;
            OverlayImage.color = new Color(color.r, color.g, color.b, alpha);
            HeadDividerLine.color = color;
            HeadLineText.color = color;            
            _synopsisHandler.SetText(sysnopsisText);
        }

        public void SetSelectionMode(bool active)
        {
            if(active)
            {
                //_canvasHandler.interactable = true;
                //_canvasHandler.blocksRaycasts = true;
                _synopsisHandler.EnableChanges();
            }
            else
            {
                //_canvasHandler.interactable = false;
                //_canvasHandler.blocksRaycasts = false;
                _synopsisHandler.DisableChanges();
            }
        }
    }
}


