﻿using DTools.Suice;

namespace ComplexUI
{
    [ImplementedBy(typeof(NewsTileFactory))]
    public interface INewsTileFactory : IProvider
    {
        void ReturnToPool(INewsTileController controller);
    }
}