﻿
using System;
using DTools.Suice;
using Prime31.ZestKit;
using UnityEngine;

namespace ComplexUI
{
    [ProvidedBy(typeof(INewsTileFactory))]
    public interface INewsTileController
    {
        Transform transform { get; }
        void Destroy();
        void SetPosition(Vector3 position);
        void SetActive(bool active);
        void SetParentTransform(Transform parentTransform, bool worldStay);
        void SetValues(float renderZ, float alpha);
        void SetTileData(Sprite sprite, Color color, string headlineText, string sysnopsisText);
        void TweenTo(Vector3 position, EaseType type, LoopType loop = LoopType.None, float duration = 1.0f);
        void ScaleTweenTo(float scale);
        Vector2 GetSize();
        void RotationTweenTo(Vector3 rotation);
        void SetSelectionMode(bool active);
        void SetupForReaderMode(bool inToReaderMode);
    }
}
