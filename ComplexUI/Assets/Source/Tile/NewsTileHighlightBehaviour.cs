﻿using System;
using System.Text;
using Prime31.ZestKit;
using TouchScript.Gestures;
using UnityEngine;
using UnityEngine.UI;

namespace ComplexUI
{
    public class NewsTileHighlightBehaviour : MonoBehaviour
    {
        private const string HIGHLIGHT_START_TAG = "<color=#FFF02EFF>";
        private const string TEXT_START_TAG = "<color=#FF5500FF>";
        private const string SHADOW_START_TAG = "<color=#00000000>";
        private const int START_TAG_LENGHT = 17;
        private const string END_TAG = "</color>";
        private const int END_TAG_LENGHT = 8;

        [SerializeField]
        private PressGesture _pressGesture;
        [SerializeField]
        private ReleaseGesture _releaseGesture;

        public PressGesture ToBeToggeledPressGesture;

        [SerializeField]
        private Text _highlightText;
        [SerializeField]
        private InputField _inputField;
        [SerializeField]
        private Text _shadowText;
        [SerializeField]
        private Text _trueText;

        private ITween<Vector3> _posTween;
        private ITween<Vector3> _scaleTween; 

        enum State
        {
            None,
            W8ing4Change,
            Changing,
        }
        State _state = State.None;

        private int _startIndex = -1;
        private int _endIndex = -1;

        private int _highlightStartIndex = -1;
        private int _hightlightEndIndex = -1;

        void OnEnable()
        {
            _pressGesture.Pressed += OnPressed;
            _releaseGesture.Released += OnReleased;
        }

        void OnDisable()
        {
            _pressGesture.Pressed -= OnPressed;
            _releaseGesture.Released -= OnReleased;
        }

        private void OnReleased(object sender, EventArgs e)
        {
            ToBeToggeledPressGesture.enabled = true;
        }

        private void OnPressed(object sender, EventArgs e)
        {
            ToBeToggeledPressGesture.enabled = false;
        }

        void LateUpdate()
        {
            switch(_state)
            {
                case State.None:
                    return;
                case State.W8ing4Change:
                    if(hasSelection)
                        _state = State.Changing;
                    break;                
                case State.Changing:
                    if (HasSelectionChanged())
                    {
                        ModifyHighlight();
                    }
                    break;
            }
        }

        public void EnableChanges()
        {
            _state = State.W8ing4Change;
        }

        public void DisableChanges()
        {
            if(_highlightStartIndex != -1 || _hightlightEndIndex != -1)
            {
                StringBuilder sb = new StringBuilder(_highlightText.text);
                RemoveDataFromStringBuilder(ref sb);
                _highlightStartIndex = -1;
                _hightlightEndIndex = -1;
                _highlightText.text = sb.ToString();
                _trueText.text = sb.ToString();
                _shadowText.text = sb.ToString();
            }
            _state = State.None;
        }

        internal void SetText(string sysnopsisText)
        {            
            _highlightText.text = sysnopsisText;
            _inputField.text = sysnopsisText;
            _shadowText.text = sysnopsisText;
            _trueText.text = sysnopsisText;
        }

        private void ModifyHighlight()
        {
            StringBuilder sb = new StringBuilder(_highlightText.text);
            if(_highlightStartIndex != -1 || _hightlightEndIndex != -1)
            {
                RemoveDataFromStringBuilder(ref sb);
            }

            int startIndex = _startIndex;
            int endIndex = _endIndex;
            if (startIndex > endIndex)
            {
                int num2 = startIndex;
                startIndex = endIndex;
                endIndex = num2;
            }

            _highlightStartIndex = startIndex;
            _hightlightEndIndex = endIndex + START_TAG_LENGHT;

            _highlightText.text = GetAddedDataFromStringBuilder(ref sb, HIGHLIGHT_START_TAG);// sb.ToString();
            RemoveDataFromStringBuilder(ref sb);
            _shadowText.text = GetAddedDataFromStringBuilder(ref sb, SHADOW_START_TAG);
            RemoveDataFromStringBuilder(ref sb);
            _trueText.text = GetAddedDataFromStringBuilder(ref sb, TEXT_START_TAG);

            
            TweenHighlight();
            
        }

        private string GetAddedDataFromStringBuilder(ref StringBuilder sb, string tag)
        {
            sb.Insert(_highlightStartIndex, tag.ToCharArray());
            sb.Insert(_hightlightEndIndex, END_TAG.ToCharArray());
            return sb.ToString();
        }

        private void RemoveDataFromStringBuilder(ref StringBuilder stringBuilder)
        {
            stringBuilder.Remove(_hightlightEndIndex, END_TAG_LENGHT);
            stringBuilder.Remove(_highlightStartIndex, START_TAG_LENGHT);
        }

        private bool HasSelectionChanged()
        {            
            if(!hasSelection)
            {
                if (_highlightStartIndex != -1 || _hightlightEndIndex != -1)
                {
                    StringBuilder sb = new StringBuilder(_highlightText.text);
                    RemoveDataFromStringBuilder(ref sb);
                    _highlightStartIndex = -1;
                    _hightlightEndIndex = -1;
                    _highlightText.text = sb.ToString();
                    _trueText.text = sb.ToString();
                    _shadowText.text = sb.ToString();
                }
                _state = State.W8ing4Change;
                return false;
            }
            int startIndex = _inputField.selectionAnchorPosition;
            int endIndex = _inputField.selectionFocusPosition;
            if (startIndex > endIndex)
            {
                int num2 = startIndex;
                startIndex = endIndex;
                endIndex = num2;
            }

            if(startIndex != _startIndex || endIndex != _endIndex)
            {
                _startIndex = startIndex;
                _endIndex = endIndex;
                return true;
            }
            return false;
        }

        private bool hasSelection
        {
            get
            {
                return _inputField.selectionFocusPosition != _inputField.selectionAnchorPosition;
            }
        }

        private void TweenHighlight()
        {
            _highlightText.transform.ZKlocalPositionTo(new Vector3(0, 0, -2))
                .setEaseType(EaseType.BounceOut)
                .setFrom(new Vector3(0, 0, 0))
                .start();
            _highlightText.transform.ZKlocalScaleTo(new Vector3(1.02f, 1.02f, 1.02f))
                    .setEaseType(EaseType.BounceOut)
                    .setFrom(new Vector3(1, 1, 1))
                    .start();
        }
    }
}