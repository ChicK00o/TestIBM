﻿using DTools.Suice;
using UnityEngine;

namespace ComplexUI
{
    [ImplementedBy(typeof(NewsTileDataProvider))]
    public interface INewsTileDataProvider
    {
        int GetMaxID { get; }

        void GetData(int newsId, out Sprite highlightSprite, out Color overlayColor, out string headline, out string synopsis);
        int GetGroupID(int newsId);
    }
}