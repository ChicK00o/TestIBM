﻿using System.Collections.Generic;
using DTools.Suice;
using UnityEngine;
using UnitySuiceCommons.EventDispatcher;
using UnitySuiceCommons.Resource;

namespace ComplexUI
{
    [Singleton]
    public class NewsTileFactory : Factory<INewsTileController>, IInitializable, INewsTileFactory
    {
        private const string NEWS_TILE_POOL_CONTAINER_NAME = "_NewsTilePool";
        private const int INITIAL_POOL_COUNT = 10;
        private const string NEWS_TILE_ASSET_PATH = "NewsTile";
        
        private readonly Queue<INewsTileController> _newsTilePool = new Queue<INewsTileController>();
        private GameObject _prefabTemplate;
        private GameObject _poolContainer;

        private readonly IUnityResources _unityResources;
        private ICustomTouchHandler _touchHandler;
        private IGlobalEventDispatcher _eventDispatcher;

        [Inject]
        public NewsTileFactory(IUnityResources unityResources, ICustomTouchHandler touchHandler,
            IGlobalEventDispatcher eventDispatcher)
        {
            _unityResources = unityResources;
            _touchHandler = touchHandler;
            _eventDispatcher = eventDispatcher;
        }

        public void Initialize()
        {
            //TODO : Optimize by just instantiating a pool object compared to creating the pool at runtime. One instantiation better than multiple instantiation
            _poolContainer = _unityResources.CreateEmptyGameObject(NEWS_TILE_POOL_CONTAINER_NAME);            
            _prefabTemplate = (GameObject)_unityResources.Load(NEWS_TILE_ASSET_PATH, typeof(GameObject));

            for (int i = 0; i < INITIAL_POOL_COUNT; i++)
            {
                _newsTilePool.Enqueue(CreateNewTileController());
            }
        }

        private NewsTileController CreateNewTileController()
        {
            GameObject gameObject = (GameObject)_unityResources.Instantiate(_prefabTemplate);

            gameObject.name = _prefabTemplate.name;
            gameObject.transform.SetParent(_poolContainer.transform,false);
            gameObject.SetActive(false);

            return new NewsTileController(this, gameObject.GetComponent<NewsTile>(), _touchHandler, _eventDispatcher);
        }

        public override INewsTileController Provide()
        {
            INewsTileController newsController = _newsTilePool.Count > 0
                ? _newsTilePool.Dequeue()
                : CreateNewTileController();

            newsController.SetActive(true);

            return newsController;
        }

        public void ReturnToPool(INewsTileController controller)
        {
            // Reset controller to default values
            controller.SetActive(false);
            controller.SetPosition(_prefabTemplate.transform.position);
            controller.SetParentTransform(_poolContainer.transform, false);

            _newsTilePool.Enqueue(controller);
        }
    }
}

