﻿
using DTools.Suice;
using UnityEngine;
using UnitySuiceCommons.Resource;

namespace ComplexUI
{
    [Singleton]
    public class NewsTileDataProvider : INewsTileDataProvider
    {
        private Color[] _colors;
        private int _maxCount;
        private const int FORCE_MAX_COUNT = 25;

        public int GetMaxID { get { return _maxCount; } }

        private IUnityResources _unityResources;

        [Inject]
        public NewsTileDataProvider(IUnityResources resources)
        {
            _unityResources = resources;
            _colors = new[]
            {Color.magenta, Color.yellow, Color.blue, Color.green, Color.Lerp(Color.yellow, Color.red, 0.6f)};
            _maxCount = FORCE_MAX_COUNT <= 0 ? 1 : FORCE_MAX_COUNT;
        }

        public void GetData(int newsId, out Sprite highlightSprite, out Color overlayColor, out string headline,
            out string synopsis)
        {
            newsId = TestAndConfirmID(newsId, out highlightSprite);
            if (highlightSprite == null)
            {
                highlightSprite =
                    _unityResources.Load<Sprite>(Constants.NEWS_IMAGES_FOLDER + Constants.NEWS_IMAGES_PREFIX + newsId);
            }

            overlayColor = _colors[GetGroupID(newsId)];

            headline = "";
            synopsis = Constants.DEFAULT_DATA_SYNOPSIS;//.Replace("<br>", "\n");
        }

        private int TestAndConfirmID(int newsId, out Sprite highlightSprite)
        {
            if (FORCE_MAX_COUNT > 0)
            {
                if (newsId > FORCE_MAX_COUNT - 1)
                    newsId = newsId%(FORCE_MAX_COUNT);
            }

            highlightSprite =
                _unityResources.Load<Sprite>(Constants.NEWS_IMAGES_FOLDER + Constants.NEWS_IMAGES_PREFIX + newsId);

            if (highlightSprite != null)
            {
                if (newsId > _maxCount)
                    _maxCount = newsId;
            }
            else
            {
                newsId = newsId%_maxCount;
            }

            return newsId;
        }

        public int GetGroupID(int newsId)
        {
            return newsId % (_colors.Length);
        }
    }    
}