﻿using System;
using Prime31.ZestKit;
using TouchScript.Gestures;
using UnityEngine;

namespace ComplexUI
{
    public interface INewsTile
    {
        GameObject gameObject { get; }        
        CanvasGroup GetGanvasHandler { get; }
        void SetTileData(Sprite sprite, Color color, string headlineText, string sysnopsisText);
        void SetSelectionMode(bool active);
        PressGesture GetPressGesture { get; }
        ReleaseGesture GetReleaseGesture { get; }
        TapGesture GetTapGesture { get; }
    }
}
