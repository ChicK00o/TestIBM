﻿using System;
using DTools.Suice;
using Prime31.ZestKit;
using TouchScript.Gestures;
using UnityEngine;
using UnityEngine.UI;
using UnitySuiceCommons.EventDispatcher;
using UnitySuiceCommons.Injector;

namespace ComplexUI
{
    public class CameraTransformHandler : MonoBehaviour, ICameraTransformHandler
    {
        public ScreenTransformGesture CameraManipulationGesture;
        public ScreenTransformGesture MultiTouchGesture;
        public float RotationSpeedX = 200f;
        public float RotationSpeedY = 100f;

        public float LimitationYMin = 77f;
        public float LimitationYMax = 288f;

        [SerializeField]
        private Transform _cameraDisplacement;

        [SerializeField]
        private Image _bgImageForReader;

        private ICustomTouchHandler _customTouchHandler;

        public void SetTouchHandler(ICustomTouchHandler customTouchHandler)
        {
            _customTouchHandler = customTouchHandler;
        }

        private void OnEnable()
        {
            CameraManipulationGesture.Transformed += TransformHandlerCameraDisplacement;            
            MultiTouchGesture.TransformCompleted += PinchGestureRecognitionEnded;
        }

        private void OnDisable()
        {
            CameraManipulationGesture.Transformed -= TransformHandlerCameraDisplacement;
            MultiTouchGesture.TransformCompleted -= PinchGestureRecognitionEnded;
        }
        
        private void TransformHandlerCameraDisplacement(object sender, EventArgs e)
        {
            if(_customTouchHandler.IsTilePressed)
            {
                SwipeMovment();
            }
            else
            {
                CameraDisplacement();
            }
        
        }

        private void CameraDisplacement()
        {
            float rotationX = CameraManipulationGesture.DeltaPosition.y / Screen.height * RotationSpeedY;
            float rotationY = CameraManipulationGesture.DeltaPosition.x / Screen.width * RotationSpeedX * -1;
            Vector3 rotation = transform.localRotation.eulerAngles;
            rotation.y += rotationY;
            rotation.x += rotationX;
            rotation.z = 0;
            transform.localRotation = Quaternion.Euler(rotation);
        }

        private void SwipeMovment()
        {
            Vector3 delta = CameraManipulationGesture.DeltaPosition;
            _customTouchHandler.Movement(delta, transform.localRotation.eulerAngles.y);
        }

        public void OnTransformToCloud(bool inToCloudMode = true)
        {
            if(inToCloudMode)
            {
                _cameraDisplacement.ZKlocalPositionTo(new Vector3(0, 0, -70), 1).setEaseType(EaseType.BounceOut).start();
            }
            else
            {
                _cameraDisplacement.ZKlocalPositionTo(new Vector3(0, 0, -35), 1).setEaseType(EaseType.BounceOut).start();
            }
        }

        public void OnTransformToAccordian()
        {
            transform.ZKlocalRotationTo(Quaternion.Euler(new Vector3(20, 39, 0)),1f).setEaseType(EaseType.CubicInOut).start();
        }

        public void SwitchToReaderMode(bool inToReader = true)
        {
            if(inToReader)
            {
                _bgImageForReader.ZKalphaTo(0.65f, 1).setDelay(0.3f).setFrom(Constants.TRANSPARENT).start();
            }
            else
            {
                _bgImageForReader.ZKalphaTo(Constants.TRANSPARENT, 1).setFrom(Constants.TRANSPARENT).start();
            }
        }

        private void PinchGestureRecognitionEnded(object sender, EventArgs e)
        {
            //Debug.Log(" pinch delta is : " + MultiTouchGesture.DeltaScale);
            if(MultiTouchGesture.DeltaScale < 1)
            {
                _customTouchHandler.Pinch(true);
            }
            if(MultiTouchGesture.DeltaScale > 1)
            {
                _customTouchHandler.Pinch(false);
            }
        }

        void LateUpdate()
        {
            Vector3 rot = transform.localRotation.eulerAngles;
            if(rot.y > LimitationYMin && rot.y < LimitationYMax)
            {
                if(rot.y <= 180)
                {
                    rot.y = LimitationYMin;
                }
                else
                {
                    rot.y = LimitationYMax;
                }
                transform.localRotation = Quaternion.Euler(rot);
            }
        }
    }
}