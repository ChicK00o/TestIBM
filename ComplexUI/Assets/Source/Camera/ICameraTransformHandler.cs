﻿namespace ComplexUI
{
    public interface ICameraTransformHandler
    {
        void SwitchToReaderMode(bool inToReaderMode = true);
        void SetTouchHandler(ICustomTouchHandler customTouchHandler);
        void OnTransformToCloud(bool inToCloudMode = true);
        void OnTransformToAccordian();
    }
}