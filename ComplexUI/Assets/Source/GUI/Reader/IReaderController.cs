﻿using DTools.Suice;

namespace ComplexUI
{
    [ImplementedBy(typeof(ReaderController))]
    public interface IReaderController
    {
        void SetTileSets(INewsTileController[] activeTiles, int[] newsIDs);
        void SetActiveTile(INewsTileController activeController);
    }
}