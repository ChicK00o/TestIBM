﻿using System;
using System.Collections.Generic;
using System.Linq;
using DTools.Suice;
using Prime31.ZestKit;
using UnityEngine;
using UnitySuiceCommons.EventDispatcher;
using UnitySuiceCommons.TaskManager;

namespace ComplexUI
{
    [Singleton]
    public class ReaderController : IReaderController
    {
        class ReaderElement
        {
            public int NewsID;
            public Vector3 Position;
            public INewsTileController RenderHolder;
            public int RowID = Int32.MaxValue;
            public int ColumnID = Int32.MaxValue;
        }

        private ReaderElement[] _buffer;

        enum State
        {
            NotActive,
            Active,
            Stable,
            Moving,
        }

        State _state = State.NotActive;

        private IReader _readerParent;
        private IUnityTaskManager _taskManager;

        private int _activeRowID;
        private int _activeColumnID;

        private INewsTileController _activeControllerToTest;
        private IAccordianController _accordianController;
        private ICameraTransformHandler _cameraTransformHandler;

        [Inject]
        public ReaderController(IReader readerParent, IUnityTaskManager taskManager,
            IAccordianController accordianController, ICameraTransformHandler cameraTransformHandler)
        {
            _readerParent = readerParent;
            _taskManager = taskManager;
            _accordianController = accordianController;
            _cameraTransformHandler = cameraTransformHandler;
        }

        public void SetTileSets(INewsTileController[] activeTiles, int[] newsIDs)
        {
            _state = State.Active;
            _buffer = new ReaderElement[activeTiles.Length];

            for (int i = 0; i < _buffer.Length; i++)
            {
                _buffer[i] = new ReaderElement()
                {
                    RenderHolder = activeTiles[i],
                    NewsID = newsIDs[i]
                };                
                _buffer[i].RenderHolder.SetParentTransform(_readerParent.transform, true);
                _buffer[i].RenderHolder.SetupForReaderMode(true);
            }

            SetRandomPostionsInXYPlane();

            SetUpTweensToNewPositons();

            _taskManager.AddTask(() =>
            {
                _state = State.Stable;
            }, TimeSpan.FromSeconds(2).TotalMilliseconds);
        }

        public void SetActiveTile(INewsTileController activeController)
        {
            _activeControllerToTest = activeController;
        }

        private void SetUpTweensToNewPositons()
        {
            for (int i = 0; i < _buffer.Length; i++)
            {
                _buffer[i].RenderHolder.TweenTo(_buffer[i].Position, EaseType.CubicIn);
                _buffer[i].RenderHolder.ScaleTweenTo(1);
                _buffer[i].RenderHolder.RotationTweenTo(new Vector3(0, 0, 0));
            }
        }

        private void SetRandomPostionsInXYPlane()
        {

            int column = 1;
            int columnCount = 1;
            int row = 1;
            int rowCount = 1;
            HashSet<string> usedColumnRow = new HashSet<string>();            

            for (int i = 0; i < _buffer.Length; i++)
            {
                if (_buffer[i].ColumnID != Int32.MaxValue)
                    continue;

                while (usedColumnRow.Contains(column.ToString() + row.ToString()))
                {
                    if (column == columnCount && row == rowCount && columnCount == rowCount)
                    {
                        columnCount++;
                        row = 1;
                        column++;
                        continue;
                    }
                    if (column == columnCount && row == rowCount && rowCount < columnCount)
                    {
                        rowCount++;
                        column = 1;
                        row++;
                        continue;
                    }
                    if (column < columnCount)
                    {
                        column++;
                        continue;
                    }
                    if (row < rowCount)
                    {
                        row++;
                        continue;
                    }
                }

                _buffer[i].ColumnID = column;
                _buffer[i].RowID = row;
                usedColumnRow.Add(column.ToString() + row.ToString());
            }

            int columnMid = columnCount/2 + 1;
            int rowMid = rowCount/2 + 1;

            for (int i = 0; i < _buffer.Length; i++)
            {
                _buffer[i].ColumnID -= columnMid;
                _buffer[i].RowID -= rowMid;
            }

            if(_activeControllerToTest != null)
            {
                SwapWithActiveNewsTile();
                _activeControllerToTest = null;
            }


            Vector2 size = _buffer[0].RenderHolder.GetSize() + new Vector2(10, 10);
            for (int i = 0; i < _buffer.Length; i++)
            {
                _buffer[i].Position = new Vector3(size.x*_buffer[i].ColumnID, size.y*_buffer[i].RowID, 0);
            }

            _activeRowID = 0;
            _activeColumnID = 0;
            ReaderElement element =
                _buffer.FirstOrDefault(ele => ele.ColumnID == _activeColumnID && ele.RowID == _activeRowID);
            element.RenderHolder.SetSelectionMode(true);
        }

        private void SwapWithActiveNewsTile()
        {
            ReaderElement currentActiveElement =
                _buffer.FirstOrDefault(ele => ele.ColumnID == _activeColumnID && ele.RowID == _activeRowID);
            ReaderElement toBeActiveElement = _buffer.FirstOrDefault(ele => ele.RenderHolder == _activeControllerToTest);
            currentActiveElement.ColumnID = toBeActiveElement.ColumnID;
            currentActiveElement.RowID = toBeActiveElement.RowID;
            toBeActiveElement.ColumnID = 0;
            toBeActiveElement.RowID = 0;
        }

        [EventListener]
        public void OnLeftMove(LeftEvent @event)
        {
            if(_state != State.Stable)
                return;
            LeftMove();
        }

        private void LeftMove()
        {
            _state = State.Moving;
            int newColumnID = _activeColumnID - 1;
            int newRowID = _activeRowID;
            Vector3 wrongMovement = new Vector3(-50, 0, 0);
            TestMovement(newColumnID, newRowID, wrongMovement);
        }

        private void TestMovement(int newColumnID, int newRowID, Vector3 wrongMovement)
        {
            ReaderElement element = _buffer.FirstOrDefault(ele => ele.ColumnID == newColumnID && ele.RowID == newRowID);

            if (element != default(ReaderElement))
            {
                Vector3 transition = Vector3.zero - element.Position;
                _activeColumnID = element.ColumnID;
                _activeRowID = element.RowID;
                for (int i = 0; i < _buffer.Length; i++)
                {
                    _buffer[i].Position = new Vector3(_buffer[i].Position.x + transition.x,
                        _buffer[i].Position.y + transition.y, 0);
                    _buffer[i].RenderHolder.TweenTo(_buffer[i].Position, EaseType.CubicInOut);
                    _buffer[i].RenderHolder.SetSelectionMode(false);
                }
                element.RenderHolder.SetSelectionMode(true);
            }
            else
            {
                for (int i = 0; i < _buffer.Length; i++)
                {
                    _buffer[i].RenderHolder.TweenTo(_buffer[i].Position - wrongMovement, EaseType.CubicInOut,
                        LoopType.PingPong, 0.5f);
                }
            }

            _taskManager.AddTask(() =>
            {
                _state = State.Stable;
            }, TimeSpan.FromSeconds(1).TotalMilliseconds);
        }

        [EventListener]
        public void OnRightMove(RightEvent @event)
        {
            if (_state != State.Stable)
                return;
            RightMove();
        }

        private void RightMove()
        {
            _state = State.Moving;
            int newColumnID = _activeColumnID + 1;
            int newRowID = _activeRowID;
            Vector3 wrongMovement = new Vector3(50, 0, 0);
            TestMovement(newColumnID, newRowID, wrongMovement);
        }

        [EventListener]
        public void OnUpMove(UpEvent @event)
        {
            if (_state != State.Stable)
                return;
            UpMove();
        }

        private void UpMove()
        {
            _state = State.Moving;
            int newColumnID = _activeColumnID;
            int newRowID = _activeRowID + 1;
            Vector3 wrongMovement = new Vector3(0, 30, 0);
            TestMovement(newColumnID, newRowID, wrongMovement);
        }

        [EventListener]
        public void OnDownMove(DownEvent @event)
        {
            if (_state != State.Stable)
                return;
            DownMove();
        }

        private void DownMove()
        {
            _state = State.Moving;
            int newColumnID = _activeColumnID;
            int newRowID = _activeRowID - 1;
            Vector3 wrongMovement = new Vector3(0, -30, 0);
            TestMovement(newColumnID, newRowID, wrongMovement);
        }

        [EventListener]
        public void OnSwipe(SwipeEvent @event)
        {
            if (_state != State.Stable)
                return;

            bool usingY = Mathf.Abs(@event.Direction.y) > Mathf.Abs(@event.Direction.x);

            if(usingY && @event.Direction.y > 0)
            {
                DownMove();
                return;
            }

            if (usingY && @event.Direction.y < 0)
            {
                UpMove();
                return;
            }

            if (@event.Direction.x > 0)
            {
                LeftMove();
                return;
            }

            if (@event.Direction.x < 0)
            {
                RightMove();
                return;
            }
        }

        [EventListener]
        public void OnPinchIn(PinchInEvent @event)
        {
            if(_state != State.Stable)
                return;
            _state = State.NotActive;
            INewsTileController[] activeTiles;
            int[] newsIDs;
            GetActiveTiles(out activeTiles, out newsIDs);
            _accordianController.SetTileSets(activeTiles, newsIDs);
            _cameraTransformHandler.SwitchToReaderMode(false);
            _cameraTransformHandler.OnTransformToAccordian();
            _buffer = null;
        }

        private void GetActiveTiles(out INewsTileController[] activeTiles, out int[] newsIDs)
        {
            activeTiles = new INewsTileController[_buffer.Length];
            newsIDs = new int[_buffer.Length];

            for (int i = 0; i < _buffer.Length; i++)
            {
                activeTiles[i] = _buffer[i].RenderHolder;
                newsIDs[i] = _buffer[i].NewsID;
                _buffer[i].RenderHolder.SetParentTransform(_accordianController.GetParentTransform(),true);
                _buffer[i].RenderHolder.ScaleTweenTo(1);
                _buffer[i].RenderHolder.RotationTweenTo(new Vector3(0, 0, 0));
                _buffer[i].RenderHolder.SetupForReaderMode(false);
            }
        }
    }
}