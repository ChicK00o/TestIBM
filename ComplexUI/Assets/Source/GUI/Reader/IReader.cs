﻿using UnityEngine;

namespace ComplexUI
{
    public interface IReader
    {
        Transform transform { get; }
    }
}