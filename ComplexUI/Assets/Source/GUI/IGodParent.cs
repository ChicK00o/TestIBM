﻿using UnityEngine;

public interface IGodParent
{
    Transform transform { get; }
    bool SetAtOrigin { get; set; }
}