﻿
using System;
using System.Collections.Generic;
using System.Linq;
using DTools.Suice;
using Prime31.ZestKit;
using UnityEngine;
using UnitySuiceCommons.EventDispatcher;
using UnitySuiceCommons.TaskManager;

namespace ComplexUI
{
    [Singleton]
    public class CloudController : ICloudController
    {
        class CloudElement
        {
            public int NewsID;
            public Vector3 Position;
            public int Layer;
            public INewsTileController RenderHolder;
        }

        private CloudElement[] _buffer;

        enum State
        {
            NotActive,
            Active,
            Stable,
            Disabled,
        }

        private State _state = State.NotActive;

        private ICloud _cloudParent;
        private INewsTileDataProvider _dataProvider;
        private IUnityTaskManager _taskManager;
        private IReaderController _readerController;
        private ICameraTransformHandler _cameraTransformHandler;
        private IAccordianController _accordianController;

        [Inject]
        public CloudController(ICloud cloudParent, INewsTileDataProvider dataProvider, IUnityTaskManager taskManager,
            IReaderController readerController, ICameraTransformHandler cameraTransformHandler, IAccordianController accordianController)
        {
            _cloudParent = cloudParent;
            _dataProvider = dataProvider;
            _taskManager = taskManager;
            _readerController = readerController;
            _cameraTransformHandler = cameraTransformHandler;
            _accordianController = accordianController;
        }

        public void SetTileSets(INewsTileController[] activeTiles, int[] newsIDs)
        {
            _state = State.Active;
            _buffer = new CloudElement[activeTiles.Length];

            for (int i = 0; i < _buffer.Length; i++)
            {
                _buffer[i] = new CloudElement
                {
                    RenderHolder = activeTiles[i],
                    NewsID = newsIDs[i]
                };
                _buffer[i].Layer = _dataProvider.GetGroupID(_buffer[i].NewsID);
                _buffer[i].Position = _cloudParent.GetHeightForLevel(_buffer[i].Layer);
                _buffer[i].RenderHolder.SetParentTransform(_cloudParent.transform, false);
            }

            _cameraTransformHandler.OnTransformToCloud();
            SetRandomPostionsInXZPlane();

            _taskManager.AddTask(SetUpTweensToNewPositons, TimeSpan.FromSeconds(1).TotalMilliseconds);

            _taskManager.AddTask(SetSiblingOrder, TimeSpan.FromSeconds(2.5f).TotalMilliseconds);

            _taskManager.AddTask(() =>
            {
                _state = State.Stable;
            }, TimeSpan.FromSeconds(3).TotalMilliseconds);
        }


        private void SetUpTweensToNewPositons()
        {
            for(int i = 0; i < _buffer.Length; i++)
            {
                _buffer[i].RenderHolder.TweenTo(_buffer[i].Position, EaseType.CubicIn);
            }
        }

        void SetSiblingOrder()
        {
            for (int i = 0; i < _buffer.Length; i++)
            {
                _buffer[i].RenderHolder.transform.SetAsFirstSibling();
            }
        }

        private void SetRandomPostionsInXZPlane()
        {
            Vector2 min, max;
            _cloudParent.GetSizes(out min, out max);
            for(int i = 0; i < _cloudParent.GetMaxLevels; i++)
            {
                float x1 = 0;
                float z1 = 0;
                if(i <= _cloudParent.GetMaxLevels / 2.0f)
                {
                    x1 = Utilities.ConvertRange(0, (_cloudParent.GetMaxLevels - 1) / 2.0f, min.x, max.x, i);
                    z1 = Utilities.ConvertRange(0, (_cloudParent.GetMaxLevels - 1) / 2.0f, min.y, max.y, i);
                }
                else
                {
                    x1 = Utilities.ConvertRange((_cloudParent.GetMaxLevels - 1) / 2.0f, (_cloudParent.GetMaxLevels - 1), max.x, min.x, i);
                    z1 = Utilities.ConvertRange((_cloudParent.GetMaxLevels - 1) / 2.0f, (_cloudParent.GetMaxLevels - 1), max.y, min.y, i);
                }
                float x2 = -1 * x1;
                float z2 = -1 * z1;

                CloudElement[] considered = _buffer.Where(element => element.Layer == i).ToArray();
                float stepX = (x2 - x1) / considered.Length;
                float stepZ = (z2 - z1) / considered.Length;

                for(int j = 0; j < considered.Length; j++)
                {
                    Vector3 pos = considered[j].Position;
                    pos.x = UnityEngine.Random.Range((j * stepX) + x1, (j * stepX) + x1 + stepX);
                    pos.z = UnityEngine.Random.Range((j * stepZ) + z1, (j * stepZ) + z1 + stepZ);
                    considered[j].Position = pos;
                }
            }

            _buffer = _buffer.OrderBy(ele => ele.Position.z).ToArray();
        }

        [EventListener]
        public void OnChangeToReaderEvent(EnableReadModeEvent @event)
        {
            if (_state != State.Stable)
                return;
            _state = State.Disabled;
            SwitchingToReader();
        }

        private void GetActiveTiles(out INewsTileController[] activeTiles, out int[] newsIDs)
        {
            activeTiles = new INewsTileController[_buffer.Length];
            newsIDs = new int[_buffer.Length];

            for (int i = 0; i < _buffer.Length; i++)
            {
                activeTiles[i] = _buffer[i].RenderHolder;
                newsIDs[i] = _buffer[i].NewsID;                
            }
        }

        [EventListener]
        public void OnDoubleTap(DoubleTapReadEvent @event)
        {
            if (_state != State.Stable)
                return;
            _state = State.Disabled;
            _readerController.SetActiveTile(@event.Controller);
            SwitchingToReader();
        }

        private void SwitchingToReader()
        {
            INewsTileController[] activeTiles;
            int[] newsIDs;
            GetActiveTiles(out activeTiles, out newsIDs);
            _cameraTransformHandler.OnTransformToCloud(false);
            _cameraTransformHandler.SwitchToReaderMode();
            _readerController.SetTileSets(activeTiles, newsIDs);
        }

        [EventListener]
        public void OnPinchIn(PinchInEvent @event)
        {
            if (_state != State.Stable)
                return;
            _state = State.NotActive;
            INewsTileController[] activeTiles;
            int[] newsIDs;
            GetActiveTiles(out activeTiles, out newsIDs);
            for (int i = 0; i < _buffer.Length; i++)
            {
                _buffer[i].RenderHolder.SetParentTransform(_accordianController.GetParentTransform(), false);
            }
            _cameraTransformHandler.OnTransformToCloud(false);
            _accordianController.SetTileSets(activeTiles, newsIDs);
        }
    }
}