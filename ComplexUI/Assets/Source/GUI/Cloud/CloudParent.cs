﻿using System;
using System.Collections;
using System.Linq;
using UnityEngine;

namespace ComplexUI
{
    public class CloudParent : MonoBehaviour, ICloud
    {
        [SerializeField]
        private Transform[] _heightObjects;
        private float[] _levelPosition;

        [SerializeField]
        private Transform _midExtreme;
        [SerializeField]
        private Transform _bottomExtreme;

        private Transform[] _childern;

        public int GetMaxLevels { get; private set; }
        
        void Awake()
        {
            _levelPosition = new float[_heightObjects.Length];
            _levelPosition[0] = _heightObjects[0].localPosition.y;
            _levelPosition[_heightObjects.Length - 1] = _heightObjects[_heightObjects.Length - 1].localPosition.y;
            float stepSize = (_levelPosition[_heightObjects.Length - 1] - _levelPosition[0]) /
                             (_heightObjects.Length - 1);
            for(int i = 1; i < _heightObjects.Length - 1; i++)
            {
                _levelPosition[i] = _levelPosition[i - 1] + Mathf.Abs(stepSize);
            }
            GetMaxLevels = _heightObjects.Length;
        }

        public Vector3 GetHeightForLevel(int level)
        {
            return new Vector3(0, _levelPosition[level], 0);
        }

        public void GetSizes(out Vector2 min, out Vector2 max)
        {
            min = new Vector2(_bottomExtreme.localPosition.x, _bottomExtreme.localPosition.z);
            max = new Vector2(_midExtreme.localPosition.x, _midExtreme.localPosition.z);
        }

        public void SetSiblingOrderTask()
        {
            _childern = GetComponentsInChildren<Transform>();
            StartCoroutine(SetSiblingValues());
        }

        private IEnumerator SetSiblingValues()
        {
            float time = 0;
            while(time <= 1.1f)
            {
                _childern = _childern.OrderBy(ele => ele.localPosition.z).ToArray();
                for (int i = 0; i < _childern.Length; i++)
                {
                    _childern[i].SetAsFirstSibling();
                }
                yield return new WaitForSeconds(0.2f);
                time += 0.2f;
            }
        }
    }
}