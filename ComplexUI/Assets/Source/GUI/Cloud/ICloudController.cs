﻿using DTools.Suice;

namespace ComplexUI
{
    [ImplementedBy(typeof(CloudController))]
    public interface ICloudController
    {
        void SetTileSets(INewsTileController[] activeTiles, int[] newsIDs);
    }
}