﻿using UnityEngine;

namespace ComplexUI
{
    public interface ICloud
    {        
        Vector3 GetHeightForLevel(int level);
        int GetMaxLevels { get; }
        Transform transform { get; }
        void GetSizes(out Vector2 min, out Vector2 max);
        void SetSiblingOrderTask();
    }
}