﻿using System;
using System.Collections.Generic;
using System.Linq;
using DTools.Suice;
using Prime31.ZestKit;
using UnityEngine;
using UnitySuiceCommons.EventDispatcher;
using UnitySuiceCommons.EventDispatcher.Unity.UnityEvent;
using UnitySuiceCommons.TaskManager;

namespace ComplexUI
{
    [Singleton]
    public class AccordainController : IAccordianController
    {
        class AccordianElement
        {
            public INewsTileController RenderHolder;
            public float CalculatedZ;
            public float RenderZ;
            public int NewsId;

            public AccordianElement(float calculatedZ)
            {
                RenderHolder = null;
                CalculatedZ = calculatedZ;
            }
        }

        private const int BUFFER_HOLD = 2;        

        private AccordianElement[] _buffer;
        private int _indexLeft;
        private int _indexRight;

        private int _initialFlow;

        private INewsTileFactory _newsTileFactory;
        private IAccordian _accordianParent;
        private INewsTileDataProvider _dataProvider;
        [Inject]
        private ICloudController _cloudController;
        [Inject]
        private IReaderController _readerController;
        private ICameraTransformHandler _cameraTransformHandler;
        private IUnityTaskManager _taskManager;

        enum State
        {
            None,
            Initial,
            Intrim,
            Stable,
            Moving,
            Disabled,
            ReEntering,
        }
        private State _state = State.None;

        private float _movementSpeed;
        private float _deAcceleration;
        private float _clamp;
        private float _clampDist;
        private float _tooLowSpeed;

        [Inject]
        public AccordainController(INewsTileFactory factory, IAccordian accordianParent,
            INewsTileDataProvider dataProvider,
            ICameraTransformHandler cameraTransformHandler, IUnityTaskManager taskManager)
        {
            _newsTileFactory = factory;
            _accordianParent = accordianParent;
            _dataProvider = dataProvider;
            _cameraTransformHandler = cameraTransformHandler;
            _taskManager = taskManager;

            _deAcceleration = 1;
            _clamp = 30;
            _clampDist = 5;
            _tooLowSpeed = 30;
        }

        [EventListener]
        public void OnStart(UnityStartEvent startEvent)
        {
            if (_accordianParent.ContainerSize < 1)
                new Exception("Container size cannot be less than one, nothing will be displayed, set the value in inspector!! :P");
            _buffer = new AccordianElement[_accordianParent.ContainerSize * 3 - 2/* 2 shared elements */];
            SetInitialBufferAndValues();
            _movementSpeed = -100;
            _state = State.Initial;            
        }        

        private void SetInitialBufferAndValues()
        {
            int maxValue = _buffer.Length - 1;
            int startWith = _accordianParent.ContainerSize - 2 /* 2 shared elements */;
            for (int i = 0; i < _buffer.Length; i++)
            {
                _buffer[i] = new AccordianElement(_accordianParent.LeftExtremePos.z + i * _accordianParent.StepSize);
                _buffer[i].NewsId = startWith;
                startWith = startWith - 1 < 0 ? maxValue : startWith - 1;
            }

            _indexRight = _buffer.Length - 1;            
            _indexLeft = 0;
        }

        [EventListener]
        public void OnAddFromLeft(AddFromLeftEvent @event)
        {
            Debug.Log("Adding from left");
            if(_state != State.Stable)
                return;
            _movementSpeed += -50;
        }

        [EventListener]
        public void OnAddFromRight(AddFromRightEvent @event)
        {
            Debug.Log("Adding from right");
            if(_state != State.Stable)
                return;
            _movementSpeed += 50;
        }

        [EventListener]
        public void OnUpdate(UnityUpdateEvent @event)
        {
            switch(_state)
            {
                case State.Initial:
                    FillInitialNews();
                    break;
                case State.Intrim:
                    StabalizeSet();
                    break;
                case State.Stable:
                    if (Math.Abs(_movementSpeed) < float.Epsilon)
                        return;
                    StableSet();
                    break;
            }           
        }

        private void StableSet()
        {
            for (int i = 0; i < _buffer.Length; i++)
            {
                AccordianElement element = _buffer[i];
                // use movement speed to set true z
                element.CalculatedZ += _movementSpeed * Time.deltaTime;

                // set render z
                bool shouldBeRendered = false;
                element.RenderZ = _accordianParent.RenderZ(element.CalculatedZ, BUFFER_HOLD, out shouldBeRendered);

                // consider adding render component if missing
                if (shouldBeRendered)
                {
                    if (element.RenderHolder == null)
                    {                        
                        INewsTileController controller = _newsTileFactory.Provide() as INewsTileController;
                        SetTileHierachy(controller);
                        SetTileData(controller, element.NewsId);
                        element.RenderHolder = controller;
                    }
                    // if render component adjust alpha accordingly
                    element.RenderHolder.SetValues(element.RenderZ, _accordianParent.AlphaZ(element.CalculatedZ));
                }
                else
                {
                    if (element.RenderHolder != null)
                    {
                        element.RenderHolder.Destroy();
                        element.RenderHolder = null;
                    }
                }
            }

            ReAdjustIndexs();

            _movementSpeed = _movementSpeed < 0 ? _movementSpeed + _deAcceleration : _movementSpeed - _deAcceleration;

            if (Mathf.Abs(_movementSpeed) < _clamp)
            {
                if(_clampDist >= Mathf.Abs(_accordianParent.RightExtremePos.z - _buffer[_indexRight].CalculatedZ))
                {
                    _movementSpeed = 0;
                    _state = State.Intrim;
                }
                else
                {
                    if(Mathf.Abs(_movementSpeed) < _tooLowSpeed)
                        _movementSpeed = _movementSpeed < 0 ? _movementSpeed - _deAcceleration : _movementSpeed + _deAcceleration;
                }
            } 
        }

        private void SetTileHierachy(INewsTileController controller)
        {
            controller.SetParentTransform(_accordianParent.transform, false);
            if (_movementSpeed > 0)
                controller.transform.SetAsLastSibling();
            else
                controller.transform.SetAsFirstSibling();
        }

        private void StabalizeSet()
        {
            _movementSpeed = 0;
            float diff = _accordianParent.RightExtremePos.z - _buffer[_indexRight].CalculatedZ;

            for (int i = 0; i < _buffer.Length; i++)
            {
                AccordianElement element = _buffer[i];
                // use movement speed to set true z
                element.CalculatedZ += diff;

                if (element.RenderHolder != null)
                {
                    bool shouldBeRendered;
                    element.RenderZ = _accordianParent.RenderZ(element.CalculatedZ, BUFFER_HOLD, out shouldBeRendered);
                    element.RenderHolder.SetValues(element.RenderZ, _accordianParent.AlphaZ(element.CalculatedZ));
                }
            }
            _state = State.Stable;
        }

        private void FillInitialNews()
        {
            for(int i = 0; i < _buffer.Length; i++)
            {
                AccordianElement element = _buffer[i];

                bool wasRendered = _accordianParent.AlphaZ(element.CalculatedZ) > 0;
                // use movement speed to set true z
                element.CalculatedZ += _movementSpeed * Time.deltaTime;

                bool willBeRendered = _accordianParent.AlphaZ(element.CalculatedZ) > 0;

                if(!wasRendered && willBeRendered)
                {
                    if (element.RenderHolder == null)
                    {
                        INewsTileController controller = _newsTileFactory.Provide() as INewsTileController;
                        SetTileHierachy(controller);
                        SetTileData(controller, element.NewsId);
                        element.RenderHolder = controller;
                    }
                }

                if(element.RenderHolder != null)
                {
                    bool shouldBeRendered;
                    element.RenderZ = _accordianParent.RenderZ(element.CalculatedZ, BUFFER_HOLD, out shouldBeRendered);
                    element.RenderHolder.SetValues(element.RenderZ, _accordianParent.AlphaZ(element.CalculatedZ));
                }
            }

            if(ReAdjustIndexs())
                _initialFlow ++;

            if(_initialFlow >= _accordianParent.ContainerSize)
                _state = State.Intrim;
        }

        private void SetTileData(INewsTileController controller, int newsId)
        {
            Sprite highlightSprite;
            Color overlayColor;
            string headline;
            string synopsis;
            _dataProvider.GetData(newsId, out highlightSprite, out overlayColor, out headline, out synopsis);
            controller.SetTileData(highlightSprite, overlayColor, headline, synopsis);
        }

        private bool ReAdjustIndexs()
        {
            //Moved Right
            if (_buffer[_indexRight].CalculatedZ <= _accordianParent.RightExtremePos.z + _accordianParent.StepSize)
            {
                _buffer[_indexRight].CalculatedZ = _buffer[_indexLeft].CalculatedZ - _accordianParent.StepSize;
                _buffer[_indexRight].NewsId = _buffer[_indexLeft].NewsId + 1;
                _indexLeft = _indexRight;
                _indexRight = _indexRight - 1 < 0 ? _buffer.Length - 1 : _indexRight - 1;
                return true;
            }

            //Moved Left
            if (_buffer[_indexLeft].CalculatedZ >= _accordianParent.LeftExtremePos.z - _accordianParent.StepSize)
            {
                _buffer[_indexLeft].CalculatedZ = _buffer[_indexRight].CalculatedZ + _accordianParent.StepSize;
                _buffer[_indexLeft].NewsId = _buffer[_indexRight].NewsId - 1 < 0 ? _dataProvider.GetMaxID : _buffer[_indexRight].NewsId - 1;
                _indexRight = _indexLeft;
                _indexLeft = _indexLeft + 1 >= _buffer.Length ?  0 : _indexLeft + 1;
                return true;
            }

            return false;
        }

        private void GetActiveTiles(out INewsTileController[] activeTiles, out int[] newsIDs)
        {
            int startIndex = _indexLeft + _accordianParent.ContainerSize - 1 >= _buffer.Length
                ? _indexLeft + _accordianParent.ContainerSize - 1 - _buffer.Length
                : _indexLeft + _accordianParent.ContainerSize - 1;

            activeTiles = new INewsTileController[_accordianParent.ContainerSize];
            newsIDs = new int[_accordianParent.ContainerSize];

            for (int i = 0; i < _accordianParent.ContainerSize; i++)
            {
                activeTiles[i] = _buffer[startIndex].RenderHolder;
                newsIDs[i] = _buffer[startIndex].NewsId;
                startIndex = startIndex + 1 < _buffer.Length ? startIndex + 1 : 0;
            }

            foreach (AccordianElement element in _buffer)
            {
                if (element.RenderHolder != null)
                {
                    if (!activeTiles.Contains(element.RenderHolder))
                    {
                        element.RenderHolder.Destroy();
                    }
                    element.RenderHolder = null;
                }
            }
        }

        private void TransformToCloud()
        {
            if (_state != State.Stable)
                return;
            _state = State.Disabled;

            INewsTileController[] activeTiles;
            int[] newsIDs;
            GetActiveTiles(out activeTiles, out newsIDs);

            _cloudController.SetTileSets(activeTiles, newsIDs);
        }

        [EventListener]
        public void TransformToCloud(TransformToCloudEvent @event)
        {
            TransformToCloud();
        }

        [EventListener]
        public void OnPinchOut(PinchOutEvent @event)
        {
            TransformToCloud();
        }
        

        [EventListener]
        public void OnChangeToReaderEvent(EnableReadModeEvent @event)
        {
            if (_state != State.Stable)
                return;
            _state = State.Disabled;

            INewsTileController[] activeTiles;
            int[] newsIDs;
            GetActiveTiles(out activeTiles, out newsIDs);
            _cameraTransformHandler.SwitchToReaderMode();
            _readerController.SetTileSets(activeTiles, newsIDs);
        }

        [EventListener]
        public void OnSwipe(SwipeEvent @event)
        {
            if (_state != State.Stable)
                return;

            if (@event.CameraY < 180 && @event.CameraY >= 0)
                @event.Direction.x *= -1;

            if (@event.Direction.x < 0)
                _movementSpeed += -1 * @event.Direction.sqrMagnitude;
            if (@event.Direction.x > 0)
                _movementSpeed += 1 * @event.Direction.sqrMagnitude;

            if(Mathf.Abs(_movementSpeed) > 150)
                _movementSpeed = _movementSpeed < 0 ? -150 : 150;
        }

        [EventListener]
        public void OnDoubleTap(DoubleTapReadEvent @event)
        {
            if (_state != State.Stable)
                return;
            _state = State.Disabled;

            INewsTileController[] activeTiles;
            int[] newsIDs;
            GetActiveTiles(out activeTiles, out newsIDs);
            _cameraTransformHandler.SwitchToReaderMode();
            _readerController.SetActiveTile(@event.Controller);
            _readerController.SetTileSets(activeTiles, newsIDs);
        }

        public void SetTileSets(INewsTileController[] activeTiles, int[] newsIDs)
        {
            _state = State.ReEntering;

            for(int i = 0; i < newsIDs.Length; i++)
            {
                AccordianElement element = _buffer.FirstOrDefault(ele => ele.NewsId == newsIDs[i]);
                element.RenderHolder = activeTiles[i];
            }

            AccordianElement[] lst = _buffer.OrderBy(ele => ele.CalculatedZ).ToArray();
            for (int i = 0; i < lst.Length; i++)
            {
                bool shouldBeRendered = false;
                _accordianParent.RenderZ(lst[i].CalculatedZ, BUFFER_HOLD, out shouldBeRendered);
                if(shouldBeRendered && lst[i].RenderHolder == null)
                {
                    INewsTileController controller = _newsTileFactory.Provide() as INewsTileController;
                    SetTileHierachy(controller);
                    SetTileData(controller, lst[i].NewsId);
                    lst[i].RenderHolder = controller;
                    lst[i].RenderHolder.SetValues(lst[i].RenderZ, _accordianParent.AlphaZ(lst[i].CalculatedZ));
                }

                if(lst[i].RenderHolder != null)
                {
                    lst[i].RenderHolder.transform.SetAsFirstSibling();
                    lst[i].RenderHolder.TweenTo(new Vector3(0, 0, lst[i].RenderZ), EaseType.CubicInOut);
                }
            }

            _taskManager.AddTask(() => { _state = State.Stable; }, TimeSpan.FromSeconds(1.5f).Milliseconds);
        }

        public Transform GetParentTransform()
        {
            return _accordianParent.transform;
        }
    }
}
