﻿using UnityEngine;

namespace ComplexUI
{
    public interface IAccordian
    {
        Transform transform { get; }
        Vector3 LeftExtremePos { get; }
        Vector3 MidLeftPos { get; }
        Vector3 MidRightPos { get; }
        Vector3 RightExtremePos { get; }
        int ContainerSize { get; }
        float StepSize { get; }
        float RenderZ(float calculatedZ, int buffer, out bool shouldBeRendered);
        float AlphaZ(float calculatedZ);        
    }
}