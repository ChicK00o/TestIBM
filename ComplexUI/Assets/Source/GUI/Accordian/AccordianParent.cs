﻿using UnityEngine;

namespace ComplexUI
{
    public class AccordianParent : MonoBehaviour, IAccordian
    {
        [SerializeField]
        private Transform _leftExtreme;
        [SerializeField]
        private Transform _midLeft;
        [SerializeField]
        private Transform _midRight;
        [SerializeField]
        private Transform _rightExtreme;
        [SerializeField]
        private int _containerSize;
        [SerializeField]
        private AnimationCurve _leftSideCurve;
        [SerializeField]
        private AnimationCurve _midCurve;
        [SerializeField]
        private AnimationCurve _rightSideCurve;
        [SerializeField]
        private AnimationCurve _completeCurve;
        [SerializeField]
        private AnimationCurve _completeAlphaCurve;

        private float _midLeftPercent;        
        private float _midRightPercent;

        private float _stepSize;
        private float _stepSizePercent;

        public Vector3 LeftExtremePos { get; private set; }
        public Vector3 MidLeftPos { get; private set; }
        public Vector3 MidRightPos { get; private set; }
        public Vector3 RightExtremePos { get; private set; }
        public int ContainerSize { get; private set; }
        public float StepSize { get { return _stepSize; } }

        void Awake()
        {
            ContainerSize = _containerSize;
            MidLeftPos = _midLeft.localPosition;
            MidRightPos = _midRight.localPosition;
            Vector3 size = MidRightPos - MidLeftPos;
            LeftExtremePos = MidLeftPos - size;
            _leftExtreme.localPosition = LeftExtremePos;
            RightExtremePos = MidRightPos + size;
            _rightExtreme.localPosition = RightExtremePos;

            _stepSize = (MidRightPos.z - MidLeftPos.z) / (_containerSize - 1);
            SetAnimationCurves();
        }

        private void SetAnimationCurves()
        {
            //compelte curve
            Keyframe[] allKeys = new Keyframe[6];
            allKeys[0].inTangent = 0;
            allKeys[0].outTangent = 0;
            allKeys[0].tangentMode = 0;
            allKeys[0].time = 0;
            allKeys[0].value = 0;
            float completeZDist = Mathf.Abs(RightExtremePos.z - LeftExtremePos.z);
            _stepSizePercent = Mathf.Abs(_stepSize)/completeZDist;

            _midLeftPercent = Mathf.Abs(MidLeftPos.z - LeftExtremePos.z) / completeZDist;
            allKeys[2].inTangent = 1f;
            allKeys[2].outTangent = 1f;
            allKeys[2].tangentMode = 0;
            allKeys[2].time = _midLeftPercent;
            allKeys[2].value = _midLeftPercent;

            _midRightPercent = 1 - Mathf.Abs(RightExtremePos.z - MidRightPos.z) / completeZDist;
            allKeys[3].inTangent = 1f;
            allKeys[3].outTangent = 1f;
            allKeys[3].tangentMode = 0;
            allKeys[3].time = _midRightPercent;
            allKeys[3].value = _midRightPercent;

            allKeys[1].inTangent = 0;
            allKeys[1].outTangent = 0;
            allKeys[1].tangentMode = 0;
            allKeys[1].time = _midLeftPercent - _stepSizePercent;
            allKeys[1].value = 0;

            allKeys[4].inTangent = 0;
            allKeys[4].outTangent = 0;
            allKeys[4].tangentMode = 0;
            allKeys[4].time = _midRightPercent + _stepSizePercent;
            allKeys[4].value = 1;

            allKeys[5].inTangent = 0;
            allKeys[5].outTangent = 0;
            allKeys[5].tangentMode = 0;
            allKeys[5].time = 1;
            allKeys[5].value = 1;

            _completeCurve.keys = allKeys;

            //Alpha Curve

            allKeys[1].time = _midLeftPercent - _stepSizePercent;

            allKeys[2].inTangent = 0;
            allKeys[2].outTangent = 0;
            allKeys[2].value = 1;

            allKeys[3].inTangent = 0;
            allKeys[3].outTangent = 0;
            allKeys[3].time = _midRightPercent;
            allKeys[3].value = 1;

            allKeys[4].time = _midRightPercent + _stepSizePercent;
            allKeys[4].value = 0;

            allKeys[5].value = 0;

            _completeAlphaCurve.keys = allKeys;

        }

        public float RenderZ(float calculatedZ, int buffer, out bool shouldBeRendered)
        {
            float value = ConvertRange01(calculatedZ);
            
            if (value > _midLeftPercent - buffer * _stepSizePercent &&
               value < _midRightPercent + buffer * _stepSizePercent)
            {
                shouldBeRendered = true;
            }
            else
            {
                shouldBeRendered = false;
            }
            return ConvertRangeOriginal(_completeCurve.Evaluate(value));
        }

        public float AlphaZ(float calculatedZ)
        {
            return _completeAlphaCurve.Evaluate(ConvertRange01(calculatedZ));
        }

        

        float ConvertRange01(float value) // value to convert
        {
            // original range
            float originalStart = LeftExtremePos.z;
            float originalEnd = RightExtremePos.z;
            // desired range
            float newStart = 0;
            float newEnd = 1;
            return Utilities.ConvertRange(originalStart, originalEnd, newStart, newEnd, value);
        }

        float ConvertRangeOriginal(float value) // value to convert
        {
            // original range
            float originalStart = 0;
            float originalEnd = 1;
            // desired range
            float newStart = LeftExtremePos.z;
            float newEnd = RightExtremePos.z;
            return Utilities.ConvertRange(originalStart, originalEnd, newStart, newEnd, value);
        }

    }
}