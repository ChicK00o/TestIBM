﻿using DTools.Suice;
using UnityEngine;

namespace ComplexUI
{
    [ImplementedBy(typeof(AccordainController))]
    public interface IAccordianController
    {
        void SetTileSets(INewsTileController[] activeTiles, int[] newsIDs);
        Transform GetParentTransform();
    }
}