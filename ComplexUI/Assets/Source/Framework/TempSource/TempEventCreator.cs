﻿using DTools.Suice;
using UnityEngine;
using UnitySuiceCommons.EventDispatcher;
using UnitySuiceCommons.EventDispatcher.Unity.UnityEvent;

namespace ComplexUI
{
    [Singleton]
    public class TempEventCreator
    {
        private IGlobalEventDispatcher _eventDispatcher;
        private AddFromLeftEvent _leftEvent;
        private AddFromRightEvent _rightEvent;

        [Inject]
        public TempEventCreator(IGlobalEventDispatcher eventDispatcher)
        {
            _eventDispatcher = eventDispatcher;
            _leftEvent = new AddFromLeftEvent();
            _rightEvent = new AddFromRightEvent();
        }

        [EventListener]
        public void OnUpdate(UnityUpdateEvent updateEvent)
        {
            if(Input.GetKeyDown(KeyCode.W))
            {
                _eventDispatcher.BroadcastEvent(_leftEvent);
            }

            if(Input.GetKeyDown(KeyCode.S))
            {
                _eventDispatcher.BroadcastEvent(_rightEvent);
            }

            if (Input.GetKeyDown(KeyCode.T))
            {
                _eventDispatcher.BroadcastEvent(new TransformToCloudEvent());
            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                _eventDispatcher.BroadcastEvent(new EnableReadModeEvent());
            }

            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                _eventDispatcher.BroadcastEvent(new UpEvent());
            }

            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                _eventDispatcher.BroadcastEvent(new DownEvent());
            }

            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                _eventDispatcher.BroadcastEvent(new RightEvent());
            }

            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                _eventDispatcher.BroadcastEvent(new LeftEvent());
            }
        }

    }
}