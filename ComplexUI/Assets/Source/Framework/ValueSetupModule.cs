﻿using UnityEngine;
using DTools.Suice;

namespace ComplexUI
{
    public class ValueSetupModule : AbstractModule
    {
        public override void Configure()
        {
            Bind<IAccordian>()
                .ToInstance(GameObject.FindObjectOfType<AccordianParent>().GetComponent<AccordianParent>());
            Bind<ICloud>().ToInstance(GameObject.FindObjectOfType<CloudParent>().GetComponent<CloudParent>());
            Bind<ICameraTransformHandler>().ToInstance(GameObject.FindObjectOfType<CameraTransformHandler>().GetComponent<CameraTransformHandler>());
            Bind<IReader>().ToInstance(GameObject.FindObjectOfType<ReaderParent>().GetComponent<ReaderParent>());
        }
    }
}