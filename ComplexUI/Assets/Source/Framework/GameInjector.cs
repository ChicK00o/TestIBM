﻿using UnitySuiceCommons.Injector;

namespace ComplexUI
{
    public class GameInjector : UnityInjector
    {
        protected override void RegisterModules()
        {
            Injector.RegisterModule(new ValueSetupModule());
        }
        protected override void SetNameSpaceStrings() { }
    }
}

