﻿using UnitySuiceCommons.EventDispatcher;

namespace ComplexUI
{
    public struct PinchInEvent : IDispatchedEvent { }
}