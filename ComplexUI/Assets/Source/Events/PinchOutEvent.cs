﻿using UnitySuiceCommons.EventDispatcher;

namespace ComplexUI
{
    public struct PinchOutEvent : IDispatchedEvent { }    
}