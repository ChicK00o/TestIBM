﻿using UnitySuiceCommons.EventDispatcher;

namespace ComplexUI
{
    public struct DoubleTapReadEvent : IDispatchedEvent
    {
        public DoubleTapReadEvent(INewsTileController controller)
        {
            Controller = controller;
        }
        public INewsTileController Controller;
    }
}
