﻿using UnitySuiceCommons.EventDispatcher;

namespace ComplexUI
{
    public struct DownEvent : IDispatchedEvent { }
}
