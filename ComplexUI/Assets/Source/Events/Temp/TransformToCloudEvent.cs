﻿using UnitySuiceCommons.EventDispatcher;

namespace ComplexUI
{
    public struct TransformToCloudEvent : IDispatchedEvent { }
}
