﻿using UnitySuiceCommons.EventDispatcher;

namespace ComplexUI
{
    public struct EnableReadModeEvent : IDispatchedEvent { }
}