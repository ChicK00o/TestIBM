﻿using UnitySuiceCommons.EventDispatcher;

namespace ComplexUI
{
    public struct AddFromRightEvent : IDispatchedEvent { }
}