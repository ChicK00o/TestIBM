﻿using UnityEngine;
using UnitySuiceCommons.EventDispatcher;

namespace ComplexUI
{
    public struct SwipeEvent : IDispatchedEvent
    {
        public Vector2 Direction;
        public float CameraY;
    }
}

