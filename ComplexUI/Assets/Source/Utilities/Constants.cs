﻿/*    
    Author - Rohit Bhosle
*/

namespace ComplexUI
{
    public static class Constants
    {
        public const string DEFAULT_DATA_SYNOPSIS =
            "\"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,\n\ntotam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae\n\nNeque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut\"";

        //Float value Constants
        public const float OPAQUE = 1f;
        public const float TRANSPARENT = 0f;

        //Path
        public const string NEWS_IMAGES_FOLDER = "NewsImages/";

        //File Prefix
        public const string NEWS_IMAGES_PREFIX = "NewsImage_";
    }
}
