﻿using Prime31.ZestKit;
using UnityEngine;

namespace ComplexUI
{
    public class TransformShakeTween : AbstractTweenable
    {
        private Transform _transform;
        private Vector3 _shakeDirection = Vector3.zero;
        private Vector3 _shakeOffset = Vector3.zero;
        private float _shakeIntensity = 0.3f;
        private float _shakeDegredation = 0.95f;


        
        public TransformShakeTween(Transform objecTransform, float shakeIntensity = 0.3f, float shakeDegredation = 0.95f, Vector3 shakeDirection = default(Vector3))
        {
            _transform = objecTransform.transform;
            _shakeIntensity = shakeIntensity;
            _shakeDegredation = shakeDegredation;
            _shakeDirection = shakeDirection.normalized;
        }

        #region AbstractTweenable

        public override bool tick()
        {
            if (_isPaused)
                return false;

            if (Mathf.Abs(_shakeIntensity) > 0f)
            {
                _shakeOffset = _shakeDirection;
                if (_shakeOffset != Vector3.zero)
                {
                    _shakeOffset.Normalize();
                }
                else
                {
                    _shakeOffset.x += Random.Range(0f, 1f) - 0.5f;
                    _shakeOffset.y += Random.Range(0f, 1f) - 0.5f;
                }

                _shakeOffset *= _shakeIntensity;
                _shakeIntensity *= -_shakeDegredation;
                if (Mathf.Abs(_shakeIntensity) <= 0.01f)
                    _shakeIntensity = 0f;

                _transform.localPosition += _shakeOffset;

                return false;
            }

            _isCurrentlyManagedByZestKit = false;
            return true;
        }

        #endregion

    }
}
