﻿using UnityEngine;

[ExecuteInEditMode]
public class RootAtOrigin : MonoBehaviour
{
    public Transform OriginReference;
    public bool RootToOrigin;

    public bool SetAtOrigin
    {
        get { return RootToOrigin; }
        set { RootToOrigin = value; }
    }

    void LateUpdate()
    {
        if(RootToOrigin)
        {
            transform.position = OriginReference.position;
            transform.rotation = OriginReference.rotation;
        }
    }

}
