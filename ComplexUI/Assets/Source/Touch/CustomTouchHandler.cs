﻿using DTools.Suice;
using UnityEngine;
using UnitySuiceCommons.EventDispatcher;

namespace ComplexUI
{
    [Singleton]
    public class CustomTouchHandler : ICustomTouchHandler, IInitializable
    {
        private ICameraTransformHandler _cameraTransformHandler;
        private SwipeEvent _swipeEventPayload;
        private IGlobalEventDispatcher _eventDispatcher;

        private bool _tilePressed;

        public bool IsTilePressed { get { return _tilePressed; } }

        [Inject]
        public CustomTouchHandler(ICameraTransformHandler cameraTransformHandler, IGlobalEventDispatcher eventDispatcher)
        {
            _cameraTransformHandler = cameraTransformHandler;
            _eventDispatcher = eventDispatcher;
            _swipeEventPayload = new SwipeEvent();
        }

        public void Initialize()
        {
            _cameraTransformHandler.SetTouchHandler(this);
        }

        public void OnPressed()
        {
            _tilePressed = true;
        }

        public void OnReleased()
        {
            _tilePressed = false;            
        }

        public void Movement(Vector3 deltaPosition, float cameraAngle)
        {
            //Debug.Log("delta recived is : " + deltaPosition);
            _swipeEventPayload.Direction = deltaPosition;
            _swipeEventPayload.CameraY = cameraAngle;
            _eventDispatcher.BroadcastEvent(_swipeEventPayload);
        }

        public void Pinch(bool isPinchIn)
        {
            if(isPinchIn)
                _eventDispatcher.BroadcastEvent(new PinchInEvent());
            else
                _eventDispatcher.BroadcastEvent(new PinchOutEvent());
        }
    }
}

