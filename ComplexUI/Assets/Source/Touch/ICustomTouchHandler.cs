﻿using DTools.Suice;
using UnityEngine;

namespace ComplexUI
{
    [ImplementedBy(typeof(CustomTouchHandler))]
    public interface ICustomTouchHandler
    {
        void OnPressed();
        void OnReleased();        
        void Pinch(bool isPinchIn);
        void Movement(Vector3 delta, float cameraAngle);
        bool IsTilePressed { get; }
    }
}