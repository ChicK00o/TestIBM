﻿using System;
using System.Collections;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;

public class ScriptableObjectFactory
{
	[MenuItem("Assets/Create/ScriptableObject")]
	public static void Create()
	{
		var assembly = GetAssembly();


		var allScriptableObjects = (from t in assembly.GetTypes()
		                            where t.IsSubclassOf(typeof(ScriptableObject))
		                            select t).ToArray();


		var window = EditorWindow.GetWindow<ScriptableObjectWindow>(true, "Create a new ScriptableObject", true);
        window.Types = allScriptableObjects;
        window.ShowPopup();
	}

	private static Assembly GetAssembly()
	{
		return Assembly.Load(new AssemblyName("Assembly-CSharp"));
	}
}