using System;

namespace DTools.Suice
{
	/// <summary>
	/// Container for a singleton instance
	/// 
	/// @author DisTurBinG
	/// </summary>
	public class SingletonProvider : Provider
	{
		internal object Instance;
		internal int SceneNumber;

		public SingletonProvider (Type providedType)
			: this (providedType, providedType)
		{
			SceneNumber = -1;
		}

		public SingletonProvider (Type providedType, int sceneNumber)
			: this (providedType, providedType)
		{
			SceneNumber = sceneNumber;
		}

		public SingletonProvider (Type providedType, Type implementedType)
			: base (providedType, implementedType)
		{
			SceneNumber = -1;
		}

		public SingletonProvider (Type providedType, Type implementedType, int sceneNumber)
			: base (providedType, implementedType)
		{
			SceneNumber = sceneNumber;
		}

		internal virtual void CreateSingletonInstance ()
		{
			if (Instance == null) {
				SetInstance (Activator.CreateInstance (ImplementedType, Dependencies));
			}
		}

		internal void SetInstance (object instance)
		{
			Instance = instance;
		}

		protected override object ProvideObject ()
		{
			return Instance;
		}
	}
}