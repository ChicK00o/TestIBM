﻿using System;

namespace DTools.Suice
{
	/// <summary>
	/// Singleton attribute flag, which marks a class should be instantiated as a singleton instance in the Injector.
	/// @author DisTurBinG
	/// Adding Scene Number, so as to intantiate it on entering a particular scene
	/// any class marked as singleton but with no id, will be instantiated by default at startup, of the program
	/// </summary>
	[AttributeUsage (AttributeTargets.Class)]

	public class Singleton : Attribute
	{
		internal readonly int SceneNumber;

		public Singleton (int sceneNumber)
		{
			SceneNumber = sceneNumber;
		}

		public Singleton ()
		{
			SceneNumber = -1;
		}
	}
}